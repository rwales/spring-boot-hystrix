package org.rwales;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

@Service
public class GreetingService {

    private static final long TEN_SECONDS_IN_MILLIS = 1_000 * 10;

    @Autowired
    private RequestScopedBean requestScopedBean;

    /*
     * Observations:
     *
     * - The default 'execution.isolation.strategy' is THREAD. In this mode, the command will execute on a worker
     * thread (within a dedicated, configurable thread-pool which can also be monitored using the dashboard).
     * BEWARE! NO REQUEST-SCOPE EXISTS ON THE WORKER THREAD!
     *
     * - The execution isolation strategy can be switched to SEMAPHORE. In this mode, the command executes on the
     * calling thread so request scope is available, but timeouts are not. This makes the command defenceless against
     * down-stream latency (SSL hand-shakes, slow DNS look-ups, etc).
     * <pre>
     *     @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
     *         @HystrixProperty(name="execution.isolation.strategy", value="SEMAPHORE")
     *     })
     * </pre>
     *
     * - Hystrix configuration is well defined here: https://github.com/Netflix/Hystrix/wiki/configuration
     * However, the property suffixes are NOT required. For example, in the docs, the command-specific property for
     * setting a timeout is 'hystrix.command.HystrixCommandKey.execution.isolation.thread.timeoutInMilliseconds' but
     * here we only need to specify 'execution.isolation.thread.timeoutInMilliseconds'
     *
     * - These properties can be extracted to YAML files for Spring boot. I suppose it would be cleaner to supply a set
     * of global properties and only override command-specific items (like the fall-back method).
     *
     * - The Hystrix dashboard is available at http://localhost:8080/hystrix
     * You must enter an event stream address of 'localhost:8080/hystrix.stream' for the dashboard to function. Also,
     * the dashboard doesn't seem to work until some hystrix events have been emitted. You can also view thread-pool
     * statuses via the dashboard which is quite nice.
     *
     * - I wonder if Spring restTemplate requires a request scope? I imagine it does since we often inject request-
     * scoped material at a very low level (e.g. request context headers). Without the ability to timeout requests,
     * hystrix is effectively neutered. We can still be good citizens to down-stream services (circuit-breaker) but
     * we have failed to implement a bulk-head pattern and remain vulnerable to down-stream latency.
     */

    /**
     * Provide a friendly greeting
     *
     * @param name your name (used to customize the greeting and method behavior)
     * @return a friendly greeting (unless your name is 'slow' or 'fail')
     */
    @HystrixCommand(commandKey = "GetGreeting", fallbackMethod = "fallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "4"),
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "60000"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "180000")},
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "30"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "180000")})
    String getGreeting(final String name) {
        if ("slow".equalsIgnoreCase(name)) {
            while (true) {
                try {
                    // This response will be slow
                    Thread.sleep(TEN_SECONDS_IN_MILLIS);
                    break;
                } catch (InterruptedException e) {
                    // Cowardly refusing to die...
                }
            }
        } else if ("fail".equalsIgnoreCase(name)) {
            throw new RuntimeException("Bad things afoot!");
        }

        final StringBuilder sb = new StringBuilder();
        sb.append("<p>[Thread ID in service: ").append(Thread.currentThread().getId()).append("]</p>");
        sb.append("<p>[RequestScopedBean ID in controller: ").append(requestScopedBeanId()).append("]</p>");
        sb.append("Hello, ").append(name).append("!");
        return sb.toString();
    }

    /**
     * Fall-back method referenced by the above hystrix command. Note, it must take the same args as the primary
     * command.
     *
     * @return a fall-back greeting (since the primary command has failed)
     */
    public String fallback(final String name) {
        return String.format("Hello %s from the fall-back method!", name);
    }

    /**
     * Safely attempt to resolve the current request scoped bean ID
     *
     * @return request scoped bean identifier (or -1 if injection failed, -2 if request context not available)
     */
    private long requestScopedBeanId() {
        if (requestScopedBean == null) {
            // Injection failure
            return -1;
        } else if (RequestContextHolder.getRequestAttributes() == null) {
            // No request context!
            return -2;
        } else {
            return requestScopedBean.getId();
        }
    }

}
