package org.rwales;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Stores unique request identifiers - each instance gets a unique identifier
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestScopedBean {

    private static final AtomicLong counter = new AtomicLong();
    private final long id;

    public RequestScopedBean() {
        id = counter.getAndIncrement();
    }

    /**
     * @return unique identifier for the current request
     */
    long getId() {
        return id;
    }
}
