package org.rwales;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @Autowired
    private RequestScopedBean requestScopedBean;

    @Autowired
    private GreetingService greetingService;

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(name = "name", defaultValue = "stranger") final String name) {
        final StringBuilder sb = new StringBuilder();
        sb.append("<p>[Thread ID in controller: ").append(Thread.currentThread().getId()).append("]</p>");
        sb.append("<p>[RequestScopedBean ID in controller: ").append(requestScopedBean.getId()).append("]</p>");
        sb.append("Service says...<hr/>");
        sb.append(greetingService.getGreeting(name));
        return sb.toString();
    }

}
